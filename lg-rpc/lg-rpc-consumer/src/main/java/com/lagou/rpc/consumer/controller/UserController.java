package com.lagou.rpc.consumer.controller;

import com.lagou.rpc.api.IUserService;
import com.lagou.rpc.consumer.proxy.RpcClientProxy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author bwcx_jzy
 * @since 2021/12/17
 */
@RestController
public class UserController {

    private final IUserService userService;

    public UserController() {
        this.userService = (IUserService) RpcClientProxy.createProxy(IUserService.class);
    }

    @RequestMapping("get_user_by_id")
    public Object getUser(Integer id) {
        return userService.getById(id);
    }
}
