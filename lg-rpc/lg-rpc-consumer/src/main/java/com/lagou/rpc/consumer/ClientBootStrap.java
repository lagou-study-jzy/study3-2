package com.lagou.rpc.consumer;

import com.lagou.rpc.consumer.server.ZookeeperServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * 测试类
 *
 * @author user
 */
@SpringBootApplication
@ServletComponentScan
public class ClientBootStrap implements CommandLineRunner {

    @Autowired
    private ZookeeperServer zookeeperServer;

    public static void main(String[] args) throws Exception {

        SpringApplication.run(ClientBootStrap.class, args);

//        IUserService userService = (IUserService) RpcClientProxy.createProxy(IUserService.class);
//        User user = userService.getById(1);
//        System.out.println(user);
    }

    @Override
    public void run(String... args) throws Exception {
        zookeeperServer.initListener();
        //zookeeperServer.initReport();
    }
}
