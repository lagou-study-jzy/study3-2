package com.lagou.rpc.api;

import com.lagou.rpc.pojo.User;

/**
 * 用户服务
 *
 * @author user
 */
public interface IUserService {

    /**
     * 根据ID查询用户
     *
     * @param id 用户ID
     * @return user
     */
    User getById(int id);
}
