package com.lagou.rpc.provider;

import com.lagou.rpc.provider.server.RpcServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author user
 */
@SpringBootApplication
public class ServerBootstrapApplication implements CommandLineRunner {


    @Autowired
    private RpcServer rpcServer;

    public static void main(String[] args) {
        SpringApplication.run(ServerBootstrapApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        String portStr = args[0];
        int port = Integer.parseInt(portStr);
        new Thread(() -> rpcServer.startServer("127.0.01", port)).start();
    }
}
